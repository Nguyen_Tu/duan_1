<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="{{ route('admin') }}">Admin</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin/video') }}">Videos</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        @auth
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin/logout') }}">Logout</a>
            </li>
        @endauth
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin/login') }}">Login</a>
            </li>
        @endguest
    </ul>
</nav>