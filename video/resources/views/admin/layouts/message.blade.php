<div class="message" id="message">
    @php
        $success = session()->get('success');
        $warnings = session()->get('warnings');
        $info = session()->get('info');
    @endphp
    @if(isset($success))
        <div class="alert alert-success">
            <strong>Success!</strong> {{ $success }}
        </div>
    @endif
    @if(isset($info))
        <div class="alert alert-info">
            <strong>Info!</strong> {{ $info }}
        </div>
    @endif
    @if(isset($warnings))
        @foreach($warnings->all() as $warning)
            <div class="alert alert-warning">
                <strong>Warning!</strong> {{ $warning }}
            </div>
        @endforeach
    @endif
    @if(isset($errors))
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                <strong>Danger!</strong> {{ $error }}
            </div>
        @endforeach
    @endif
</div>