<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Html::script('js/app.js') }}
    {{ Html::style('css/app.css') }}
    @yield('head')
</head>
<body>
@include('admin.layouts.header')
@include('admin.layouts.message')
<div class="container" style="margin-top:80px">
    @yield('body')
</div>
</body>
</html>