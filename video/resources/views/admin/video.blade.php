@extends('admin.layouts.app')

@section('head')
    <title>Videos</title>
@endsection

@section('body')
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Image</th>
            <th>Source</th>
            <th>View Count</th>
            <th>Created At</th>
            <th>Updated At</th>
        </tr>
        </thead>
        <tbody>
        <?php /** @var \App\Models\Video[] $videos */ ?>
        @foreach($videos as $video)
            <tr>
                <td>{{ $video->id }}</td>
                <td>{{ $video->image }}</td>
                <td>{{ $video->source }}</td>
                <td>{{ $video->view_count }}</td>
                <td>{{ $video->created_at }}</td>
                <td>{{ $video->updated_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection