@extends('admin.layouts.app')

@section('head')
    <title>Login</title>
@endsection

@section('body')
    <form method="post" action="{{ route('admin/authenticate') }}">
        {{ Form::token() }}
        <div class="form-group">
            <label>Email</label>
            <input name="email" class="form-control" type="email" title="Email" required>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input name="password" class="form-control" type="password" title="Password" required>
        </div>
        <input class="btn btn-primary" type="submit" value="Login">
    </form>
@endsection