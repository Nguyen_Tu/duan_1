<?php

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('admin');
});

Route::get('login', [
    'uses' => 'Admin\UserController@login',
    'as' => 'login'
]);

Route::prefix('admin')->group(function () {
    Route::get('login', [
        'uses' => 'Admin\UserController@login',
        'as' => 'admin/login'
    ]);
    Route::post('authenticate', [
        'uses' => 'Admin\UserController@authenticate',
        'as' => 'admin/authenticate'
    ]);
    Route::middleware(['auth', 'web', 'role:1'])->group(function () {
        Route::get('/', [
            'uses' => 'Admin\UserController@index',
            'as' => 'admin'
        ]);
        Route::get('logout', [
            'uses' => 'Admin\UserController@logout',
            'as' => 'admin/logout'
        ]);
        Route::get('video', [
            'uses' => 'Admin\VideoController@index',
            'as' => 'admin/video'
        ]);
    });
});
