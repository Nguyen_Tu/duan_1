<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
//    Route::post('video/upload', [
//        'uses' => 'Api\VideoController@upload',
//        'as' => 'video/upload'
//    ]);
});

Route::post('video/upload', [
    'uses' => 'Api\VideoController@upload',
    'as' => 'api/video/upload'
]);

Route::get('video/get', [
    'uses' => 'Api\VideoController@get',
    'as' => 'api/video/get'
]);

Route::middleware('auth.basic')->get('/user/authenticate', [
    'uses' => 'Api\UserController@authenticate',
    'as' => 'api/user/authenticate'
]);
