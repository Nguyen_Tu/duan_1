<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 13/06/2018
 * Time: 13:58
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App\Models
 * @property int id
 * @property string name
 */
class Role extends Model
{
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
