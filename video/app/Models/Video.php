<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 13/06/2018
 * Time: 13:18
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Video
 * @package App\Models
 * @property-read int id
 * @property string image
 * @property string source
 * @property int view_count
 * @property-read Carbon created_at
 * @property-read Carbon updated_at
 */
class Video extends Model
{
    protected $fillable = ['image', 'source', 'view_count'];
}
