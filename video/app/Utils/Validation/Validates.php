<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 10/04/2018
 * Time: 16:13
 */

namespace App\Utils\Validation;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

trait Validates
{
    use ValidatesRequests;

    /**
     * @param Request $request
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @param bool $throw
     * @throws \Exception
     */
    public function validateRequest(Request $request, array $rules, array $messages = [], array $customAttributes = [], $throw = true)
    {
        if ($throw) {
            $this->validateArray($request->all(), $rules, $messages, $customAttributes);
        } else {
            $this->validate($request, $rules, $messages, $customAttributes);
        }
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @throws \Exception
     */
    public function validateArray(array $data, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = $this->getValidationFactory()->make($data, $rules, $messages, $customAttributes);
        if ($validator->fails()) {
            throw new \Exception($validator->errors()->first());
        }
    }
}
