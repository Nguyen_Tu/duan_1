<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 13/06/2018
 * Time: 11:26
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\VideoUploadRequest;
use App\Models\Video;
use Illuminate\Support\Facades\Input;

class VideoController extends Controller
{
    public function upload(VideoUploadRequest $request)
    {
        $now = now()->toDateTimeString();
        $videos = $request->getVideos();
        foreach ($videos as &$video) {
            $video = array_merge($video, ['created_at' => $now, 'updated_at' => $now]);
        }
        Video::query()->insert($videos);
        return response('success');
    }

    public function get()
    {
        $validator = $this->getValidationFactory()->make(Input::get(), [
            'sort' => 'string|in:view_count,updated_at,created_at',
            'offset' => 'integer|gte:0',
            'limit' => 'integer|gte:0'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toArray(), 400);
        }
        $sort = Input::get('sort', 'view_count');
        $offset = Input::get('offset', 0);
        $limit = Input::get('limit', 50);
        $query = Video::query()->offset($offset)->orderByDesc($sort);
        if ($limit) {
            $query->limit($limit);
        }
        return response()->json($query->get()->toArray());
    }
}
