<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 13/06/2018
 * Time: 14:24
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
        return response(Hash::make($request->user()->id));
    }
}
