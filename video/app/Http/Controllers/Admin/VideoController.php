<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 14/06/2018
 * Time: 09:58
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Support\Facades\Input;

class VideoController extends Controller
{
    public function index()
    {
        $validator = $this->getValidationFactory()->make(Input::get(), [
            'page' => 'int'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator->errors()->toArray());
        }
        $page = Input::get('page', 1);
        return view('admin.video', [
            'videos' => Video::query()->offset(($page - 1) * 20)->limit(20)->get()
        ]);
    }
}
