<?php
/**
 * Created by PhpStorm.
 * User: doanphihung
 * Date: 13/06/2018
 * Time: 16:29
 */

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AuthenticateRequest;
use Illuminate\Support\Facades\Auth;

class UserController
{
    public function login()
    {
        return view('admin.login');
    }

    public function logout()
    {
        Auth::logout();
        return back()->with(['success' => 'You have logged out.']);
    }

    public function authenticate(AuthenticateRequest $request)
    {
        if (Auth::attempt($request->getCredentials())) {
            return redirect()->route('admin')->with(['success' => 'You have logged in.']);
        }
        return back()->withErrors(['msg' => 'Incorrect email or password.']);
    }

    public function index()
    {
        return view('admin.index');
    }
}
