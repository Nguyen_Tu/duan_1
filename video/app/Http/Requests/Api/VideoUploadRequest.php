<?php

namespace App\Http\Requests\Api;

class VideoUploadRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
//        /** @var User $user */
//        $user = $this->user();
//        return $user && $user->role->id == 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'videos' => 'array|required',
            'videos.*.image' => 'url|required',
            'videos.*.source' => 'url|required|distinct|unique:videos,source',
            'videos.*.view_count' => 'integer|required|gte:0'
        ];
    }

    /**
     * @return array
     */
    public function getVideos()
    {
        return $this->get('videos');
    }
}
