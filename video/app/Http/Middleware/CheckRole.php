<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param int $roleId
     * @return mixed
     */
    public function handle($request, Closure $next, $roleId)
    {
        /** @var User|null $user */
        $user = $request->user();
        if (!$user) {
            return redirect()->route('login')->withErrors(['msg' => "Unauthorized."]);
        } elseif ($user->role->id != $roleId) {
            return redirect()->route('login')->withErrors(['msg' => "Access denied."]);
        }
        return $next($request);
    }
}
